//
//  IUMAMapsVC.h
//  IUMAMapsVC
//
//  Created by Christian Reyes Nicholas on 08/03/16.
//  Copyright (c) 2016 jose maria quinteiro gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <IUMARootFrostedFramework/IUMARootFrostedFramework.h>

@interface IUMAMapsVC : UIViewController<IUMAViewControllerProtocol>

@property (nonatomic, strong) Product_categories *category;

@end
