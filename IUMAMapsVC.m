 //
//  IUMAMapsVC.m
//  IUMAMapsVC
//
//  Created by Christian Reyes Nicholas on 08/03/16.
//  Copyright (c) 2016 jose maria quinteiro gonzalez. All rights reserved.
//

#import "IUMAMapsVC.h"
#import <MapKit/MapKit.h>
#import "Products+Annotation.h"

@interface IUMAMapsVC ()
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

@implementation IUMAMapsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSArray *products= [[Modelo singleton] getProductsFromCategoryWithoutRoot:self.category];
    
    [self.mapView addAnnotations:products];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
